// npm init -y//   to initialize the JS.
//npm install express mongoose//  to install express and mongoose

/*
Mini Activity:
	create an expressJS designated to port 4000
	create a new route with endpoint /hello and with the method GET.
*/
// const express = require("express");
// const app = express();
// app.use(express.json());
// const port = 4000;
// app.get("/hello", (req, res) => {
// 	res.send("Hello World");
// })
// app.listen(port, () => console.log(`Server is running at port: ${port}`));
//////////////////////////////////////////////////////////////////////////

///Mongoose Topic Proper/// 
//package that will allow us to create schemas to model our data structures.
//also has access to a number of methods to manipulate our database.
const express = require("express");
const mongoose = require("mongoose");
const app = express();
app.use(express.json());
const port = 4000;
/*
	Syntax: mongoose.connect("<MongoDB Atlas Connection String")
*/
//MongoDB connection.
mongoose.connect("mongodb+srv://admin:admin123@batch204.nanl81q.mongodb.net/B204-to-dos?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
//set notifications for connection to check if it's a suceess or a failure to see if we are connected.
let db = mongoose.connection;
//if a connection error occured show "connectin error"
db.on("error", console.error.bind(console, "Connection Error"));				//"error" is not defined it's an EVENT that will bind the message to tell you a bad authentication error.
//if the connection is successful show "We are connected to the cloud database."
db.once("open", () => console.log("We are connected to the cloud database."))	//"open" is not defined it's an EVENT that will bind the message.
//Mongoose Schema 
//Schemas determine the structure of the documents to be written in the database.
//Acts as a blueprints to our data.
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"				//default value that we set if we will not provide details for status.
	}
});
//Models -needs to be created after Schema above
//allows us to access the models for CRUD operation.
//1st parameter e.g. "Task" indicates the "collection" to where we will store the data, not reserve word var.
//1st paramater should be in singular form,
//2nd parameter e.g taskSchema is used to specify the schema/blueprint of the docs, not reserve word var.
	//that will be stored in the MongoDB collection.
const Task = mongoose.model("Task", taskSchema);  //"Task" is the collection name, it will show 'tasks' in MONGODB and POSTMAN, mongodDB will convert it to plural and in small letters.
app.get("/hello", (req, res) => {
	res.send("Hello World");
})
//Create a route that will allow us to create a task
//ROUTE
app.post("/tasks", (req, res) => {
	console.log(req.body)										//req.body is from postman, body, raw, JSON
	Task.findOne({name: req.body.name}, (err, result) => {		//to check if there is a duplicate, not user defined 1st para handles error, 2nd para handles tasks or results.			
		if (result !== null && result.name == req.body.name) {	//name: req.body means we will search for a name inside an object from POSTMAN which should show in our collection in MONGODB and should show on the TERMINAL too.
			return res.send("Duplicate Task Found");
		} else {
			let newTask = new Task({
				name: req.body.name
			});
			newTask.save((saveErr, savedTask)=> {				//save will save the new info in our database. not user defined 1st para handles error, 2nd para handles tasks or results.			
				if(saveErr) {
				return console.error(saveErr)
				} else {
				return res.status(201).send("new Task created")
				}
			})
		}
	});	
})
//GET/Retrieve al the tasks, we may use the same endpoint as long as the different METHOD.
app.get("/tasks", (req, res) => { 
	Task.find({}, (err, result) => {		//empty {} since we will be sort and select from all our data/documents
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})
////////////////////////////////ACTIVITY/////////////////////////////////
/////will be using the same listen here for the discussions and express, mongoose and mongoDB connection/////
//Schema
const UserSchema = new mongoose.Schema({
		username: String,
		password: String	
})
//User Model
const Signup = mongoose.model("Signup", UserSchema);

//ROUTE post
// app.post("/signup", (req, res) => {
// 	res.send("New User registered")
// })

app.post("/signup", (req, res) => {
	let newSignup = new Signup({
		username: req.body.username,
		password: req.body.password
	});
	newSignup.save((saveErr, saveSignup) => {
		if(saveErr) {
			return console.log(saveErr)
		} else {
			return res.status(201).send("New User registered")
			}
		})
	})



////////////////////////////////LISTEN TO ALL/////////////////////////////////////////
app.listen(port, () => console.log(`Server is running at port: ${port}`));

///////////////////////////////END///////////////////////////////////////////
// app.post("/tasks", (req, res) => {
// 	console.log(req.body)										//req.body is from postman, body, raw, JSON
// 	Task.findOne({name: req.body.name}, (err, result) => {		//to check if there is a duplicate, not user defined 1st para handles error, 2nd para handles tasks or results.			
// 		if (result !== null && result.name == req.body.name) {	//name: req.body means we will search for a name inside an object from POSTMAN which should show in our collection in MONGODB and should show on the TERMINAL too.
// 			return res.send("Duplicate Task Found");
// 		} else {
// 			let newTask = new Task({
// 				name: req.body.name
// 			});
// 			newTask.save((saveErr, savedTask)=> {				//save will save the new info in our database. not user defined 1st para handles error, 2nd para handles tasks or results.			
// 				if(saveErr) {
// 				return console.error(saveErr)
// 				} else {
// 				return res.status(201).send("new Task created")
// 				}
// 			})
// 		}
// 	});	
// })

// app.post("/signup", (req, res) => {
// 	let newSignup = new Signup({
// 		username: req.body.username,
// 		password: req.body.password
// 	});
// 	newSignup.save((saveErr, saveSignup) => {
// 		if(saveErr) {
// 			return console.log(saveErr)
// 		} else {
// 			return res.status(201).send("New User registered")
// 			}
// 		})
// 	})

